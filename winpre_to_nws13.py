"""usage: winpre_to_nws13.py [-h] -w WIN [WIN ...] -p PRE [PRE ...] -o OUTPUT
                          [-g GROUP [GROUP ...]] [-a] [-v]

Convert a series of OWI WIN/PRE files to an ADCIRC NWS13 Netcdf file.

optional arguments:
  -h, --help            show this help message and exit
  -w WIN [WIN ...], --win WIN [WIN ...]
                        Input WIN file(s)
  -p PRE [PRE ...], --pre PRE [PRE ...]
                        Input PRE file(s)
  -o OUTPUT, --output OUTPUT
                        Output file path
  -g GROUP [GROUP ...], --group GROUP [GROUP ...]
                        Input group name(s)
  -a, --append          If set, do not clobber the existing file, but append groups to it.
  -v, --verbose         Verbose log output

"""
from owiWinPre import *
import xarray as xr
import numpy as np
import os
import netCDF4
import argparse


# Fancy command line argument parsing setup
parser = argparse.ArgumentParser(
    description="Convert a series of OWI WIN/PRE files to an ADCIRC NWS13 Netcdf file.",
    formatter_class=argparse.RawTextHelpFormatter,
)
parser.add_argument(
    "-w", "--win", type=str, nargs="+", help="Input WIN file(s)", required=True
)
parser.add_argument(
    "-p", "--pre", type=str, nargs="+", help="Input PRE file(s)", required=True
)
parser.add_argument("-o", "--output", type=str, help="Output file path", required=True)
parser.add_argument(
    "-g",
    "--group",
    type=str,
    nargs="+",
    help="Input group name(s)",
    required=False,
    default=["Main"],
)
parser.add_argument(
    "-a",
    "--append",
    action="store_true",
    help="If set, do not clobber the existing file, but append groups to it.",
    required=False,
    default=False,
)
parser.add_argument(
    "-v",
    "--verbose",
    action="store_true",
    help="Verbose log output",
    required=False,
    default=False,
)


def add_global_nws13(
    output,
    group_order,
    institution="Oceanweather Inc. (OWI)",
    conventions=["CF-1.6", "OWI-NWS13"],
):
    with netCDF4.Dataset(output, "a") as nc:
        nc.setncattr("group_order", " ".join(group_order))
        nc.setncattr("institution", institution)
        nc.setncattr("conventions", " ".join(conventions))


def winpre2nws13(inwinfiles, inprefiles, ingroups, output, mode="w", verbose=False):
    if mode == "a":
        data = netCDF4.Dataset(output)
        group_order = data.group_order.split()
        rank = len(group_order) + 1
        outgroups = group_order + ingroups
        data.close()
    else:
        rank = 1
        outgroups = ingroups
    for inwin, inpre, group in zip(inwinfiles, inprefiles, ingroups):
        if verbose:
            print(f"Reading {group}, Rank {rank} WIN")
            print(f"Win {inwin}, Pre {inpre}")
        wind = xr.concat(
            list(iter_file_xrdataset(inwin, ftype="win")),
            dim="time",
            coords="different",
            data_vars="different",
        )
        # for i, iwin in enumerate(iter_file_xrdataset(inwin)):
        #     if i > 0:
        #         wind = xr.concat((wind, iwin), dim="time")
        #     else:
        #         wind = iwin
        print("Wind Dataset", wind)
        if verbose:
            print(f"Writing WIN {group}, Rank {rank} to {output} with mode={mode}")
        to_nws13_group(output, group, rank, wind=wind, mode=mode)
        mode = "a"
        del wind
        if verbose:
            print(f"Reading {group}, Rank {rank} PRE")
        pres = xr.concat(
            list(iter_file_xrdataset(inpre, ftype="pre")),
            dim="time",
            coords="different",
            data_vars="different",
        )
        # for i, iwin in enumerate(iter_file_xrdataset(inpre)):
        #     if i > 0:
        #         pres = xr.concat((wind, iwin), dim="time")
        #     else:
        #         pres = iwin
        if verbose:
            print("Pressure Dataset", pres)
            print(f"Writing PRE {group}, Rank {rank} to {output} with mode={mode}")
        to_nws13_group(output, group, rank, pre=pres, mode=mode)
        rank += 1
        del pres
    if verbose:
        print(
            "Writing global attributes, group_order='{0}'".format(" ".join(outgroups))
        )
    add_global_nws13(output, outgroups)


if __name__ == "__main__":
    inputs = parser.parse_args()
    if inputs.verbose:
        print(inputs)
    inwinfiles = inputs.win
    inprefiles = inputs.pre
    ingroups = inputs.group
    output = inputs.output
    if inputs.append:
        mode = "a"
    else:
        mode = "w"
    # TODO check that the file inputs exist
    winpre2nws13(
        inwinfiles, inprefiles, ingroups, output, mode=mode, verbose=inputs.verbose
    )
